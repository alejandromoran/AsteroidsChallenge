﻿package  {
	
	import flash.display.MovieClip;
	
	public class Fondo extends MovieClip {
		
		
		public function Fondo(stage)
		{
			gotoAndStop(Math.floor(Math.random()*(5)+1));
			if (stage.numChildren > 0)
			{
				stage.addChildAt(this, stage.numChildren - 1);
			}
			else
			{
				stage.addChild(this);
			}
			
			this.x=0;
			this.y=0;
		}
	}
	
}
