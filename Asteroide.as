﻿package
{
	
	import flash.display.MovieClip;
	import flash.events.*;
	import flash.display.Loader;
	import flash.net.URLRequest;
	import AsteroidsGame;
	
	public class Asteroide extends MovieClip {
		
		private var speed;
		private var escena;
		private var tipo;
		private var puntos:int;
		
		public function Asteroide(stage,tipo,x,y) {
			
			stage.addChild(this);
			escena=stage;
			this.tipo=tipo;
			var rand=Math.round(Math.random()*2+1);
			
			if(rand==1)
			{
				this.rotation=Math.random()*360;
			}
			
			if(rand==2)
			{
				this.rotation=Math.random()*360*-1;
			}
			
			if(tipo==1)
			{
				this.puntos=20;
				this.addChild(new Asteroide1());
				this.x=Math.round(Math.random()*600);
				this.y=Math.round(Math.random()*600);
				speed=4;
			}
			
			if(tipo==2)
			{
				this.puntos=50;
				this.addChild(new Asteroide2());
				this.x=x;
				this.y=y;
				speed=6;
			}
			
			if(tipo==3)
			{
				this.puntos=100;
				this.addChild(new Asteroide3());
				speed=7;
				this.x=x;
				this.y=y;
			}
			
			addEventListener(Event.ENTER_FRAME, enterFrameEvent);
			
		}
		public function selfDestruct()
		{
			//This is a method that do the self destruction of asteroid
			escena.removeChild(this);
			
		}
		public function asteroideAlcanzado()
		{

			if(tipo==1)
			{
				var ast1=new Asteroide(escena,2,this.x,this.y);
				var ast2=new Asteroide(escena,2,this.x,this.y);
				this.removeChildAt(0);
				AsteroidsGame.instance.updateScore(getScore());
				escena.removeChild(this);
			}
			
			if(tipo==2)
			{
				var ast3=new Asteroide(escena,3,this.x,this.y);
				var ast4=new Asteroide(escena,3,this.x,this.y);
				this.removeChildAt(0);
				AsteroidsGame.instance.updateScore(getScore());
				escena.removeChild(this);
			}
			
			if(tipo==3)
			{
				this.removeChildAt(0);
				AsteroidsGame.instance.updateScore(getScore());
				escena.removeChild(this);
			}
			
		}
		
		public function getScore():int
		{
			
			return puntos;
			
		}
		
		public function getTipo():int
		{
			
			return tipo;
			
		}
		
		private function enterFrameEvent(e:Event)
		{
			
			this.x += Math.sin(this.rotation*(Math.PI/180))*speed;
			this.y += Math.cos(this.rotation*(Math.PI/180))*speed*-1;
			
			if (this.x > escena.stageWidth)
			{
				this.x = 0;
			}
			else if (this.x < 0)
			{
				this.x = escena.stageWidth;
			}
		
			if (this.y > escena.stageHeight)
			{
				this.y = 0;
			}
			else if (this.y < 0)
			{
				this.y = escena.stageHeight;
			}
			
		}
		
	}
	
}