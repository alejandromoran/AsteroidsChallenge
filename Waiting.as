﻿package  {
	
	import flash.display.MovieClip;
	import flash.utils.Timer;
	import flash.events.Event;
	import flash.events.TimerEvent;
	
	public class Waiting extends MovieClip {
		
		private var temporizador:Timer;
		private var contador;
		public function Waiting(stage) {
			name="waitScreen";
			contador=0;
			tres.visible=false;
			dos.visible=false;
			uno.visible=false;
			tres.x=(stage.width*0.5) - (tres.width/2);
			tres.y= (stage.height*0.5) - (tres.height/2);
			dos.x=(stage.width*0.5) - (dos.width/2);
			dos.y=(stage.height*0.5) - (dos.height/2);
			uno.x= (stage.width*0.5) - (uno.width/2);
			uno.y= (stage.height*0.5) - (uno.height/2);
			stage.addChildAt(this,stage.numChildren);
			
			temporizador = new Timer(1000,4);
			temporizador.addEventListener(TimerEvent.TIMER, temporizadorHandler, false, 0, true);
			temporizador.start();
		}
		public function startTimer():void
		{
			temporizador.removeEventListener(TimerEvent.TIMER, temporizadorHandler);
			temporizador.start();
		}
		private function temporizadorHandler(e:TimerEvent):void
		{
			if(contador==0)
			{
				tres.visible=true;
				dos.visible=false;
				uno.visible=false;
			}
			if(contador==1)
			{
				tres.visible=false;
				dos.visible=true;
				uno.visible=false;
			}
			if(contador==2)
			{
				tres.visible=false;
				dos.visible=false;
				uno.visible=true;
				

			}
			if(contador==3)
			{
				uno.visible=false;
				contador=0;
				stage.removeChild(this);
				dispatchEvent(new Event("waitingComplete"));
				
			}
			contador++;
			
		}
	}
	
}
