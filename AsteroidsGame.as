﻿package 
{

	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	import flash.events.Event;
	import flash.ui.Mouse;
	import flash.display.StageDisplayState;
	import flash.display.Stage;
	import flash.events.TimerEvent;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.net.URLRequest;
	import flash.media.SoundTransform;
	import flash.media.SoundLoaderContext;
	
	public class AsteroidsGame extends MovieClip
	{

		private var puntuacionTotal:int = 0;
		private var escena;
		private var asteroidsBelt:AsteroidsBelt;
		private var muerto:Boolean = false;
		private var unaNave:Nave;
		private var unFondo;
		private var nuevoJuego:NewGame;
		private var unDisparo:Disparo;
		private static var _instance:AsteroidsGame;
		private var miBala:Disparo;
		private var secureZone : SecureZoneSquare;
		private var nombre;
		private var naveCreada : Boolean = false;
		
private var count :int = 1;
		public var sonido:Sound;
		public var canal:SoundChannel;

		public function AsteroidsGame()
		{
			_instance = this;
			escena = stage;
			nuevoJuego = new NewGame(stage);
		}

		public function setNombre(nombre):void
		{
			this.nombre = nombre;
		}
		public function getNombre():String
		{
			return nombre;
		}
		function loadBSO(ruta:String)
		{
			var trans:SoundTransform = new SoundTransform(0.1,0);
			var url:URLRequest = new URLRequest(ruta);

			sonido = new Sound();
			canal = new SoundChannel();

			sonido.load(url);
			canal = sonido.play(0,1,trans);
		}
		public function getPuntuacionTotal():int
		{
			return puntuacionTotal;
		}
		public static function get instance():AsteroidsGame
		{
			return _instance;
		}

		public function reset():void
		{
			//loadBSO("music/BSOGame.mp3");
			
			puntuacionTotal = 0;
			
			unFondo = new Fondo(escena);
			naveCreada = false;
			asteroidsBelt = new AsteroidsBelt(escena);			
			updateScore(0);
			addEventListener(Event.ENTER_FRAME, enterFrameEvent);
		}
		
		
		function getGoodPosition() : void
		{
			var possx : int;
			var possy : int;
			possx = Math.floor(Math.random() * escena.stageWidth);
			possy = Math.floor(Math.random() * escena.stageHeight);
			secureZone = new SecureZoneSquare();
			secureZone.alpha = .2;
			escena.addChild(secureZone);
			secureZone.x = possx;
			secureZone.y = possy;
			var zonaBuena : Boolean = false;
			for (var i=0; i<escena.numChildren; i++)
			{
				if (escena.getChildAt(i) is Asteroide)
				{
					if (escena.getChildAt(i).hitTestObject(secureZone))
					{
						zonaBuena = false;
						escena.removeChild(secureZone);
						trace ("Colisión en la zona: x="+possx+" y="+possy);
						return;
					}
					else
					{
						zonaBuena = true;
					}
				}
			}
			
			if (zonaBuena)
			{
				trace ("Zona buena para establecer la nave: x="+possx+" y="+possy);
				unaNave = new Nave(escena,possx,possy);
				naveCreada = true;
				escena.removeChild(secureZone);
				
			}
			else
			{
				trace ("Zona mala, llamada recursiva");
				getGoodPosition();
			}
			
		}
		
		function CreateShip() : void
		{
			getGoodPosition();
		}

		public function enterFrameEvent(e:Event):void
		{	
			
			if(asteroidsBelt.getAsteroidsCount()==0 && naveCreada)
			{
				trace ("Flag 1");
				
				asteroidsBelt.cambiarNivel();
				
				unaNave.quitarListener();
				escena.removeChild(unaNave);
				naveCreada = false;
				unFondo = new Fondo(escena);
			}			
			
			if (asteroidsBelt.getBeltStatus())
			{
				if (!naveCreada)
				{
					CreateShip();
				}
				
			}
			
			updateLevel();
			
			if (muerto)
			{
				gameOver();
				muerto = false;
			}
			
			if (naveCreada)
			{
				detectarColision();
				detectarColisionNave();
			}
			
		}

		public function updateLevel():void
		{

			unFondo.lvlTxt.text = asteroidsBelt.getLvl();

		}

		public function updateScore(score):void
		{

			puntuacionTotal +=  score;
			unFondo.puntosTxt.text = puntuacionTotal;

		}
		
		public function gameOver():void
		{
			//canal.stop();
			
			asteroidsBelt.destruirBelt();
			new GameOver(escena);

		}

		public function clickStart(e:Event):void
		{

			gotoAndStop(1);
			reset();

		}

		public function detectarColision():void
		{

			for (var i=0; i<escena.numChildren; i++)
			{
				if (escena.getChildAt(i) is Disparo)
				{
					var miBala = escena.getChildAt(i);
					for (var e=0; e<escena.numChildren; e++)
					{
						if (escena.getChildAt(e) is Asteroide)
						{
							//
							var objAsteroide = escena.getChildAt(e);
							//
							if (objAsteroide.hitTestObject(miBala))
							{
								miBala.selfDestruct();
								asteroidsBelt.asteroidPetado();

								objAsteroide.asteroideAlcanzado();
							}
						}
					}
				}
			}

		}

		public function detectarColisionNave():void
		{
			for (var i=0; i<escena.numChildren; i++)
			{
				// For each asteroid
				if (escena.getChildAt(i) is Asteroide)
				{
					for (var h=0; h<escena.getChildAt(i).getChildAt(0).numChildren; h++)
					{

						if (escena.getChildAt(i).getChildAt(0).getChildAt(h) is AsteroidCollisionPoint)
						{
							for (var b=0; b<unaNave.numChildren; b++)
							{
								// For each ship collision point
								if (unaNave.getChildAt(b) is ShipCollisionPoint)
								{
									
									if (unaNave.getChildAt(b).hitTestObject(escena.getChildAt(i).getChildAt(0).getChildAt(h)))
									{
										unaNave.quitarListener();
										escena.removeChild(escena.getChildAt(i));
										muerto = true;
										
										return;
									}
								}
							}
						}
					}

				}
			}
		}

	}

}