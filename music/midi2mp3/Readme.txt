====================================================================  
Direct MIDI to MP3 Converter 6, Release date: 20 May 2010
====================================================================  

Copyright (C) 2004-2010 PistonSoft
All Rights Reserved

www:    http://www.pistonsoft.com
e-mail: support@pistonsoft.com
=============================================================

Welcome to use Direct MIDI to MP3 Converter.
This document contains the following topics:

 1. Description
 2. Installation & Uninstallation
 3. How to Buy
 4. License agreement
 5. Contact information

=============================================================


1. DESCRIPTION
================================

Direct MIDI to MP3 Converter is a fast audio utility that allows you to convert MIDI files to MP3, WAV, WMA and OGG formats.
Our audio converter can quick render the MIDI file into an audio format that you can burn on an audio CD and play on a regular CD player.
An essential difference between Direct MIDI to MP3 Converter and other similar programs is that our MID Converter provides direct MIDI conversion (rendering) without sound recording.
The main advantages of direct MIDI conversion are the CD audio quality, the conversion speed and the silence during conversion.
You don't need to listen to the MIDI music and adjust recording level while converting.
Just drag and drop your MIDI files and the rest are done automatically by Direct MIDI to MP3 Converter. Conversion speed is up to 10 times faster then original midi file time!
Musicians can use own SF2 soundfonts for rendering and change the instruments quality. 
A built-in audio player (with trackbar) helps you pre-listen to MIDI, MID, RMI or Karaoke files before converting. And the built-in easy-to-use OGG/WMA/MP3 Tag Editor helps you name your MP3, WMA or OGG tracks.
The batch mode helps you simultaneously convert multiple MIDI tracks to MP3.
The batch MIDI to WAVE conversion is very useful for creating musical collections on an audio CD.
The resulting WAV or MP3 files can then be burned to an audio CD using any CD recording program.
Or you can download it to your Apple iPod or other portable MP3 player.
Using the Tempo change feature, you can create an album of your favorite MIDI or Karaoke MIDI compositions performed in any tempo.
Quality options are adjustable. You can convert your favorite mobile phone MIDI ringtones to MP3 with adjustable reverberation. 
Direct MIDI to MP3 Converter is a great choice for MIDI artists, and amateur or professional musicians who compose MIDI music using digital pianos, keyboards or music sequencers and need a tool to convert MIDI (MID, RMI, KAR) files to popular audio formats.
Also, our multilingual interface program is perfect for people who want to record MIDI, MID, RMI, KAR files on an audio CD.
All popular MIDI formats are supported: MIDI, RIFF MIDI and Karaoke MIDI.
We are the only company which has a full featured trial of our MIDI converting software, conversion time is not limited.

Direct MIDI to MP3 Converter main features:

- Quick midi rendering engine up to 10 times faster then original midi file time;
- Audio CD quality because of internal conversion without recording;
- SF2 soundfonts support;
- Adjustable Midi tempo;
- Adjustable reverberation control;
- Batch conversion mode that save your time;
- Most popular MIDI formats supported;
- Adjustable qualities and bitrates;
- High quality build-in MIDI player with trackbar;
- Full featured internal OGG/WMA/MP3 Tag editor;
- Full support of ID3 MP3 tag;
- Full support of WMA tag;
- Full support of OGG tag;
- Multilingual interface;
- Hot keys for all operations;
- An easy-to-use Drag and Drop interface.


2. INSTALLATION & UNINSTALLATION
================================

  - Installation.
    Run the installation program and follow the instructions that appear on the screen.
 
  - Uninstallation.
    Run the uninstallation program.
    Or open the Control Panel folder and double-click the Add/Remove Programs icon.
    Select Direct MIDI to MP3 Converter from the list and then click the Add/Remove button.
    Follow the instructions that appear on the screen.


3. HOW TO BUY
================================

Direct MIDI to MP3 Converter is a shareware program, which means that you can use it freely for evaluation purposes,
before registration you can only use it for 30 days.
If you want to use Direct MIDI to MP3 Converter with no time and functional restrictions, please register your copy. 

The registration fee is ONLY $29.95. 
Direct MIDI to MP3 Converter is now sold on the Internet only.
Please just click the 'Buy Now' button on the Trial version notification page
or you may also go to our homepage http://www.pistonsoft.com to buy it.

Benefits of Registration include:

  - Full unlimited product usage.
  - Free updates.
  - Free technical support.
  - You will use it without functional restrictions.

Please visit our homepage http://www.pistonsoft.com for more details.


4. LICENSE AGREEMENT
================================

Direct MIDI to MP3 Converter is available for a free 30-days evaluation period.
Users who decide to continue using the program must purchase and register the software.
The trial version may be freely distributed, provided the distribution package
is not modified.


5. CONTACT INFORMATION
================================

If you don't get your registration code in a timely manner or if you've lost it, please let us know.
email: support@pistonsoft.com

For more info, please visit our website:
http://www.pistonsoft.com


=============================================================
Copyright (C) 2004-2010 PistonSoft
All Rights Reserved
=============================================================