=======================================================================  
Direct Audio Converter & CD Ripper 2.0, Release date: 17 May 2010
=======================================================================  

Copyright (C) 2005-2010Piston Software.
All Rights Reserved.

www:    http://www.pistonsoft.com
e-mail: support@pistonsoft.com
=============================================================

Welcome to use Direct Audio Converter & CD Ripper.
This document contains the following topics:

 1. Description
 2. Installation & Uninstallation
 3. How to Buy
 4. License agreement
 5. Contact information

=============================================================


1. DESCRIPTION
================================

Direct Audio Converter & CD Ripper is a very useful tool to convert audio files between various audio formats
and rip CD audio tracks directly to MP3, WMA, WAV, OGG, FLAC, Monkey's Audio APE or MusePack MPC.
You can convert directly from one audio format to another (like MP3 to WAV, MP3 to WMA, MP3 to OGG, WAV to MP3, etc.).
You can convert directly from one format to another using different encoding parameters and presets.
You can use embedded presets or make your own. With Direct Audio Converter & CD Ripper you can grab and rip audio files from audio CD.
Direct Audio Converter & CD Ripper can connect to freedb online database and automatic download information about audio CD.

Features:
  - Convert between MP3, WMA, WAV, OGG, FLAC, Monkey's Audio APE, MusePack MPC formats directly.
  - Additional input formats MP1, MP2, CDA, WavPack.
  - Pre-define output qualities to quickly set and manage the parameters for conversion.
  - You can redefine output qualities or make your own.
  - Exact metadata tag conversion from one audio format to another.
  - You can edit metadata tag for each audio file before conversion.
  - Direct Audio Converter & CD Ripper add to Windows Explorer context menu "Convert to..." option.
  - Support drag & drop audio files to converter list directly.
  - Rip audio CD tracks to variety of audio formats directly
    CD to MP3, CD to WMA, CD to WAV, CD to OGG, CD to FLAC, CD to APE, CD to MPC. 
  - Reliable and high performance CD ripping engine, featured with jitter correction to compensate reading errors. 
  - Retrieve disc album information from freedb online database.
  - With build-in MP3 player, you can pre-listen any audio files or CD tracks.
  - Full support ID3v1.1, ID3v2, APE 1.0, APE 2.0, OGG and WMA TAGs. 


2. INSTALLATION & UNINSTALLATION
================================

  - Installation.
    Run the installation program, follow the instructions that appear on the screen.
 
  - Uninstallation.
    Run the uninstallation program.
    Or open the Control Panel folder and double-click the Add/Remove Programs icon.
    Select Direct Audio Converter & CD Ripper from the list, then click the Add/Remove button.
    Follow the instructions that appear on the screen.


3. HOW TO BUY
================================

Direct Audio Converter & CD Ripper is a shareware program, which means that you can use it freely for evaluation purposes,
before registration you can only use it for 30 days.
If you want to use Direct Audio Converter & CD Ripper with no time and functional restrictions, please register your copy. 

The registration fee is ONLY $24.95. 
Direct Audio Converter & CD Ripper is now sold on the Internet only.
Please just click the 'Buy Now' button on the Trial version notification page
or you may also go to our homepage http://www.pistonsoft.com to buy it.

Benefits of Registration include:

  - Full unlimited product usage.
  - Free updates.
  - Free technical support.
  - You will use it without functional restrictions.

Please visit our homepage http://www.pistonsoft.com for more details.


4. LICENSE AGREEMENT
================================

Direct Audio Converter & CD Ripper is available for a free 30-days evaluation period.
Users who decide to continue using the program must purchase and register the software.
The trial version may be freely distributed, provided the distribution package
is not modified.


5. CONTACT INFORMATION
================================

If you don't get your registration code in a timely manner or if you've lost it, please let us know.
email: support@pistonsoft.com

For more info, please visit our website:
http://www.pistonsoft.com


=============================================================
Copyright (C) 2010 Piston Software
All rights reserved.
=============================================================