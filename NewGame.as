﻿package  {
	
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.net.URLRequest;
	import flash.media.SoundTransform;
	
	public class NewGame extends MovieClip {
		private var escena;
		
		public var sonido:Sound; 
    	public var canal:SoundChannel; 

		public function NewGame(stage) {
			escena=stage;
			loadBSO("music/BSOEntrada.mp3");
			
			
			stage.addChild(this);
			this.startGame.addEventListener(MouseEvent.CLICK, startGameEvent);
		}
		function loadBSO(ruta:String)
		{
			var trans:SoundTransform = new SoundTransform(0.1,0);
			var url:URLRequest = new URLRequest(ruta);
			
			sonido = new Sound();
			canal = new SoundChannel();
			
			sonido.load(url)
			
			canal = sonido.play(0, 1, trans);
		}
		public function startGameEvent(e:MouseEvent):void
		{
			canal.stop();
			escena.removeChild(this);
			
			AsteroidsGame.instance.setNombre(userName.text);
			AsteroidsGame.instance.reset();
			
		}
	}
	
}
