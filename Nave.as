﻿package 
{

	import flash.display.MovieClip;
	import flash.events.*;
	import flash.utils.Timer;
	import flash.events.TimerEvent;


	public class Nave extends MovieClip
	{

		private var up:Boolean;
		private var down:Boolean;
		private var right:Boolean;
		private var left:Boolean;
		private var space:Boolean;
		private var escena;
		private var speed:Number = new Number(0);
		private var fireTimer:Timer;
		private var canFire:Boolean;
		private var maxVelocity:int;
		private var minSpeed:int;

		public function Nave(stage,posx,posy)
		{
			this.name = "miNave";
			this.escena = stage;
			stage.addChild(this);
			this.x = posx;
			this.y = posy;
			maxVelocity = 12;
			minSpeed = -6;
			fireTimer = new Timer(250,1);
			fireTimer.addEventListener(TimerEvent.TIMER, fireTimerHandler, false, 0, true);
			fireTimer.start();
			stage.addEventListener(KeyboardEvent.KEY_DOWN, keyDownEvent);
			stage.addEventListener(KeyboardEvent.KEY_UP, keyUpEvent);
			addEventListener(Event.ENTER_FRAME, enterFrameEvent);
		}

		private function fireTimerHandler(e:TimerEvent):void
		{
			canFire = true;
		}
		public function quitarListener()
		{
			stage.removeEventListener(KeyboardEvent.KEY_DOWN, keyDownEvent);
			stage.removeEventListener(KeyboardEvent.KEY_UP, keyUpEvent);
			removeEventListener(Event.ENTER_FRAME, enterFrameEvent);
		}
		public function disparar()
		{

			if (canFire)
			{
				escena.addChild(new Disparo(escena,this.x,this.y,this.rotation,maxVelocity));
				canFire = false;
				fireTimer.start();
			}
		}
		public function enterFrameEvent(e:Event):void
		{

			if (right)
			{
				this.rotation +=  8;
			}
			if (left)
			{
				this.rotation -=  8;
			}
			if (up)
			{
				this.gotoAndPlay(2);
				if (speed<maxVelocity)
				{
					speed +=  0.5;
				}
			}
			if (down)
			{
				if (speed>0)
				{
					speed -=  0.5;
				}
				else
				{
					if (speed>minSpeed)
					{
						speed -=  0.5;
					}			
				}
			}
			this.x += Math.sin(this.rotation*(Math.PI/180))*speed;
			this.y += Math.cos(this.rotation*(Math.PI/180))*speed*-1;
			
			
			if (space)
			{
				disparar();
			}
			if (this.x > escena.stageWidth)
			{
				this.x = 0;
			}
			else if (this.x < 0)
			{
				this.x = escena.stageWidth;

			}
			if (this.y > escena.stageHeight)
			{
				this.y = 0;
			}
			else if (this.y < 0)
			{
				this.y = escena.stageHeight;

			}
		}
		public function keyDownEvent(event:KeyboardEvent):void
		{
			if (event.keyCode == 38)
			{
				up = true;
			}
			if (event.keyCode == 40)
			{
				down = true;
			}
			if (event.keyCode == 37)
			{
				left = true;
			}
			if (event.keyCode == 39)
			{
				right = true;
			}
			if (event.keyCode == 32)
			{
				space = true;
			}
		}
		function keyUpEvent(event:KeyboardEvent):void
		{
			if (event.keyCode == 38)
			{
				up = false;
			}
			if (event.keyCode == 40)
			{
				down = false;
			}
			if (event.keyCode == 37)
			{
				left = false;
			}
			if (event.keyCode == 39)
			{
				right = false;
			}
			if (event.keyCode == 32)
			{
				space = false;
			}
		}
	}

}