﻿package
{
	import flash.display.StageDisplayState;
	import flash.events.MouseEvent;
	import flash.display.MovieClip;
	import AsteroidsGame;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLVariables;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequestMethod;
	import flash.events.*;
	import fl.controls.DataGrid;
	import flash.events.FullScreenEvent; 
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.net.URLRequest;
	import flash.media.SoundTransform;
	
	public class GameOver extends MovieClip {
		
		private var dg:DataGrid;
		private var escena;
		private var nombre;
		public var sonido:Sound; 
		public function GameOver(stage) {
			this.nombre=AsteroidsGame.instance.getNombre();
			escena=stage;
			stage.addChild(this);
			
			this.puntosTotales.text = String(AsteroidsGame.instance.getPuntuacionTotal());
			this.tryAgain.addEventListener(MouseEvent.CLICK, tryAgainEvent);
			sendData();
			
			
			
		}
		
		public function tryAgainEvent(e:MouseEvent):void
		{
			escena.removeChild(this);
			AsteroidsGame.instance.reset();
		} 
		
        private function sendData()
		{
           var myData:URLRequest = new URLRequest("http://asteroidschallenge.com/data/setData.php");
		   myData.method = URLRequestMethod.POST;
		   var variables:URLVariables = new URLVariables();
           variables.score = AsteroidsGame.instance.getPuntuacionTotal();
		   variables.username = AsteroidsGame.instance.getNombre();
		   myData.data = variables
		   var loader:URLLoader = new URLLoader()
           loader.dataFormat = URLLoaderDataFormat.VARIABLES
           loader.load(myData) 
        }
	}
}