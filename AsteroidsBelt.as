﻿package 
{
	
	import flash.events.Event;
	
	public class AsteroidsBelt {
		private var escena;
		private var nivel=1;
		private var numeroAsteroides;
		private var contadorClips;
		private var contadorAsteroides;
		private var cambioNivel=false;
		private var wait;
		private var beltStatus:Boolean = false;
		private var procederCambioNivel:Boolean = false;
		
		public function AsteroidsBelt(stage)
		{
			
			escena=stage;
			numeroAsteroides=4;
			crearBelt();
			
		}

		public function getLvl():int
		{
			return nivel;
		}
		public function destruirBelt():void
		{
			while(escena.numChildren > 0) 
			{
				if(escena.removeChildAt(escena.numChildren -1 ) is Asteroide)
				{
					escena.removeChildAt(escena.numChildren -1 );
				}
			}
			
		}
		public function waitingDone(e:Event)
		{
			
			if(nivel>1)
			{
				numeroAsteroides=numeroAsteroides+2;
			}
			for(var i=0;i<numeroAsteroides;i++)
			{
				var unAsteroide=new Asteroide(escena,1,0,0);
			}
			
			contadorAsteroides=numeroAsteroides*7;
			beltStatus = true;
		}
		public function getBeltStatus() : Boolean
		{
			return beltStatus;
		}
		public function crearBelt():void
		{
			
			wait = new Waiting(escena);
			wait.addEventListener("waitingComplete", waitingDone);

		}
		public function getAsteroidsCount() : int
		{
			return contadorAsteroides;
		}
		public function asteroidPetado()
		{
			contadorAsteroides--;
		}
		public function cambiarNivel():void
		{
			beltStatus = false;
			contadorAsteroides = -1;
			nivel++;
			crearBelt();
		}


	}
	
}
