﻿package  {
	
	import flash.display.MovieClip;
	import flash.events.*;
	import flash.display.Stage;
	import flash.net.URLRequest;
	import flash.media.SoundChannel;
	import flash.utils.Timer;
   	import flash.media.Sound;

	public class Disparo extends MovieClip {
		
		public var escena;
		private var speed;
		private var tiempoBala:Timer;
		private var existeBala:Boolean;
		public function Disparo(stage,x,y,rotation,maxVelocity)
		{
			escena=stage;
			existeBala=true;
			stage.addChild(this);
			this.x=x;
			this.y=y;
			speed=maxVelocity;
			this.rotation=rotation;
			tiempoBala = new Timer(650,1);
			tiempoBala.addEventListener(TimerEvent.TIMER, tiempoBalaHandler, false, 0, true);
			tiempoBala.start(); 

			stage.addEventListener(Event.ENTER_FRAME, enterFrameEvent);
		}
		public function enterFrameEvent(e:Event)
		{
			recorrido();
		}
		public function selfDestruct()
		{
			if(escena.contains(this))
			{
				tiempoBala.stop();
				tiempoBala.removeEventListener(TimerEvent.TIMER, tiempoBalaHandler);
				escena.removeEventListener(Event.ENTER_FRAME, enterFrameEvent);
				//escena.removeChild(escena.getChildByName(this.name));
				escena.removeChild(this);
			}	
		}
		
		public function tiempoBalaHandler(e:TimerEvent)
		{	
			selfDestruct();
		}
		
		public function recorrido()
		{
			
			this.x += Math.sin(this.rotation*(Math.PI/180))*(speed+3);
			this.y += Math.cos(this.rotation*(Math.PI/180))*(speed+3)*-1;
			
			if (this.x > escena.stageWidth)
				this.x = 0;
			else if (this.x < 0)
				this.x = escena.stageWidth;
 
			if (this.y > escena.stageHeight)
				this.y = 0;
			else if (this.y < 0)
				this.y = escena.stageHeight;
		}
		
	}
	
}
