# AsteroidsChallenge
AsteroidsChallenge space game made in 2009 with ActionScript 3.0 

[AsteroidsChallenge live demo](https://alejandromoran.github.io/AsteroidsChallenge)

# Preview

YouTube video demo:

[![AsteroidsChallenge demo in YouTube](http://img.youtube.com/vi/WxIcH3kNad4/0.jpg)](http://www.youtube.com/watch?v=WxIcH3kNad4)

Gameplay Screenshots:

![1](https://cloud.githubusercontent.com/assets/1541798/9703458/078c09fc-5485-11e5-8eaa-c51efad59a24.png)
![2](https://cloud.githubusercontent.com/assets/1541798/9703459/0b5ae5d0-5485-11e5-969c-4378c1b3218a.png)
![3](https://cloud.githubusercontent.com/assets/1541798/9703460/0c6abf22-5485-11e5-8d42-1d1195e69d90.png)
![4](https://cloud.githubusercontent.com/assets/1541798/9703461/0d0ac8f0-5485-11e5-95af-0e4188d7c669.png)
